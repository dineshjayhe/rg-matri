import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>RegionMatrimony.com</h1>
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Millions of Indians found their perfect match here!
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
         You too can find a life partner
        </a>
      </header>
    </div>
  );
}

export default App;
